/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstracttest;

/**
 *
 * @author tez
 */

abstract class Perimiters {
    protected float perimiter(float... a) {
        float ret = 0;
        for (float i:a)
            ret+=i;
        return ret;
    };
    abstract float field(float... a);
}

class Square extends Perimiters {

    public Square() {
    }

    public Square(float a) {
        System.out.println(perimiter(a));
        System.out.println(field(a));
    }
    
    @Override
    protected float perimiter(float... a) {
        return a[0]*4;    
    }
    
    @Override
    float field(float... a) {
        return (float)Math.pow(a[0], 2);
    }
    
}

class RectangleTrapeze extends Perimiters {
    
    private float calcH(float... a) {
        return (float)Math.pow((Math.pow(a[2],2) - Math.pow(a[0]-a[1],2)), 0.5);
    }
    
    @Override
    float field(float... a) {
        return (a[0]+a[1])/2 * calcH(a);
    }
    
}

class Rectangle extends Perimiters {

    @Override
    protected float perimiter(float... a) {
       return (2*a[0] + 2*a[1]); 
    }

    @Override
    float field(float... a) {
        return a[0]*a[1];
    }
    
}

public class AbstractTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Perimiters square = new Square(5);
       Perimiters rectangle = new Rectangle();
       System.out.println(rectangle.perimiter(5,4));
       System.out.println(rectangle.field(5,4));
       Perimiters trapeze = new RectangleTrapeze();
       System.out.println("Trapez " + trapeze.perimiter(5,4,3,(float)2.7));
       System.out.println(trapeze.field(5,4,3));
       
       Rectangle rectangle2 = new Rectangle();
       System.out.println(rectangle2.perimiter(5,4));
       System.out.println(rectangle2.field(5,4));
    }
    
}
