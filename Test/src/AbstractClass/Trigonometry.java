/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractClass;

/**
 *
 * @author tez
 */
public abstract class Trigonometry {
    public final float sin(int a, int c) {
        return (float)a/c;
    }
    
    public float cos(int b, int c) {
        return (float)b/c;
    }
    
    protected final float tg(int a, int b) {
        return a/b;
    }
    
//    protected  final float cot(int b, int a) {
//        return b/a;
//    }
    protected abstract float cot(int b, int a);
   
}
